# pySmartHome

This is the home of pySmartHome - a MicroPython and ESP32 based private Smart Home under your control.

The following sensors are currently integrated: BME280 temperature, humidity, pressure sensor; PIR motion sensor; relay.



Figure of ESP32: ![ESP32](./figures/ESP32.jpeg "ESP32")

Figure of Node-Red Flow: ![Node-Red Flow](./figures/node-red_flow.png "Node-Red Flow")

Figure of resulting Node-Red UI: Figure of Node-Red Flow: ![Node-Red Flow](./figures/node-red_ui.png "Node-Red Flow")


## Set up of ESP32, Linux, IDE

// Install ESP32 Python pip packages

// had to install as sudo, as otherwise the ESP32 could not be connected

```
pip install esptool
```

// Check the tool with the following command

```
esptool.py
```

// Download latest MiroPython version for ESP32

<https://micropython.org/download/#esp32>

// Check if ESP32 is connected via serial port

`dmesg | grep tty`

// Check which devices are connected to the pc in the bash
`#!/bin/bash
for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev); do
    (
        syspath="${sysdevpath%/dev}"
        devname="$(udevadm info -q name -p $syspath)"
        [[ "$devname" == "bus/"* ]] && exit
        eval "$(udevadm info -q property --export -p $syspath)"
        [[ -z "$ID_SERIAL" ]] && exit
        echo "/dev/$devname - $ID_SERIAL"
    )
done
`

If you are putting MicroPython on your board for the first time then you should first erase the entire flash using:

```
esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash
```

// From then on program the firmware starting at address 0x1000:

```
esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 esp32spiram-idf3-20200902-v1.13.bin
```

// If your are having trouble with permissions, fix access rights in Fedora as follow:

Error appeared: [Errno 13] could not open port /dev/ttyUSB0: [Errno 13] Keine Berechtigung: '/dev/ttyUSB0'

sudo usermod -a -G dialout $User

To proof if your account was added to the right group, run `getent group dialout` and check if the output, which looks like `dialout:x:18:username` can be found in the file `/etc/group`.

// Sill having trouble with permissions and you are getting errors such as: `ampy.pyboard.PyboardError: failed to access /dev/ttyUSB0`
Check the permission with `getfacl /dev/ttyUSB0` and adjust them with `sudo chmod 777 /dev/ttyUSB0`.

// find MicroPython packages on PyPi Link: https://pypi.org/search/?c=Programming+Language+%3A%3A+Python+%3A%3A+Implementation+%3A%3A+MicroPython&o=&q=&page=3

// Install additional packages from PyPi
// https://pypi.org/project/micropython-umqtt.simple2/

```
import upip
upip.install("micropython-umqtt.simple2")
upip.install("micropython-umqtt.robust2")
```

// PyCharm with MicroPython Plugin

<https://medium.com/@andymule/micropython-in-pycharms-basic-setup-9169b497ec8a>
