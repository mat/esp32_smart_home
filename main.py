__author__ = "T. Masiak"
__credits__ = ["-", "-"]
__license__ = "MIT"
__version__ = "0.1"
__maintainer__ = "T. Masiak"
__email__ = "masiaktobias@gmail.com"
__status__ = "Production"

import os
import machine
import time

print("start esp32")
print("CPU frequency: ", machine.freq())
print("ID: ", machine.unique_id())
print("Software version: ", os.uname().release)

# exec(open('./temperature.py').read(),globals())
# print("temperature.py started")
exec(open("./mqtt_sub_pub.py").read(), globals())
print("mqtt_sub_pub.py started")

time.sleep(1)

exec(open("./temp_pressure_humidity.py").read(), globals())
print("temp_pressure_humidity.py started")

time.sleep(1)

#exec(open("./PIR_sensor.py").read(), globals())
#print("PIR_sensor.py started")

#exec(open("./deepsleep.py").read(), globals())
#print("deepsleep.py started")


