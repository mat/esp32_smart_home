__author__ = "T. Masiak"
__credits__ = ["-", "-"]
__license__ = "MIT"
__version__ = "0.1"
__maintainer__ = "T. Masiak"
__email__ = "masiaktobias@gmail.com"
__status__ = "Production"

from machine import Pin
import time
import _thread
from mqtt_sub_pub import MQTT_PUB

class MOTION_SENSOR:
    def __init__(self):
        self.motion = False
        self.pir = Pin(4, Pin.IN)
        self.pir.irq(trigger=Pin.IRQ_RISING, handler=self.interrupt_handler)
        self.inst_pub2 = MQTT_PUB()

    def interrupt_handler(self, pin):
        self.motion = True
        self.interrupt_pin = pin

    def dectect_motion(self):
        while True:
            if self.motion:
                #print("Motion detected! Interrupt caused by:", self.interrupt_pin)
                self.inst_pub2.mqtt_pub(pub_topic="esp32_external/motion", pub_value=self.motion)
                time.sleep(10) # The PIR motion sensor has a default delay time of about 10 seconds. This means that it won’t be triggered before 10 seconds have passed since the last trigger.
                #print("Motion stopped!")
                self.motion = False
                self.inst_pub2.mqtt_pub(pub_topic="esp32_external/motion", pub_value=self.motion)
                time.sleep(1)


if __name__ == "__main__":
    inst_motion = MOTION_SENSOR()
    _thread.start_new_thread(inst_motion.dectect_motion, ())