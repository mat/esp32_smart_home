# This file is executed on every boot (including wake-boot from deepsleep)
import esp

esp.osdebug(
    0
)  # Redirect vendor O/S debugging messages to UART(0). Turn off vendor O/S debugging messages with "None".

#set the cpu frequency
import machine
machine.freq()
machine.freq(160000000)

# check the reason of the restart
import machine
if machine.reset_cause() == machine.DEEPSLEEP_RESET:
    print('Woke up from a deep sleep')
else:
    print('Power on or hard reset')

# read json parameters from file
import ujson
f = open("paramters.json")
obj_read = f.read()
f.close()
json_read = ujson.loads(obj_read)


def wifi_connect():
    import network

    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print("connecting to wifi network")
        wlan.connect(json_read["ssid"], json_read["password"])
        while not wlan.isconnected():
            pass
    print("network config:", wlan.ifconfig())


wifi_connect()
