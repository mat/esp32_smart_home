__author__ = "T. Masiak"
__credits__ = ["-", "-"]
__license__ = "MIT"
__version__ = "0.1"
__maintainer__ = "T. Masiak"
__email__ = "masiaktobias@gmail.com"
__status__ = "Production"

"""
# Install once umqqt libs
import upip
upip.install("micropython-umqtt.simple2")
upip.install("micropython-umqtt.robust2")
"""

import time
from lib.umqtt.robust2 import MQTTClient
import _thread
import time
from gpios import GPIO_CONTROL
import uping


class MQTT_SUB:
    def __init__(self):
        self.msg_value = ""
        self.inst_GPIO = GPIO_CONTROL()

        server = "deskminiserver"
        self.client_sub = MQTTClient("umqtt_client_sub_1", server)
        self.client_sub.set_callback(self.sub_cb)
        self.client_sub.connect()

    def sub_cb(self, topic, msg, retain, dup):
        print((topic, msg, retain, dup))
        self.msg_value = msg
        self.msg_value = self.msg_value.decode("utf-8")
        print("value: ", self.msg_value)
        self.check_topic()

    def mqtt_sub(self, blocking_method=False):
        self.client_sub.subscribe(b"esp_subpub")
        try:
            while True:
                if blocking_method:
                    self.client_sub.wait_msg()
                else:
                    self.client_sub.check_msg()
        except:
            self.client_sub.disconnect()

    def check_topic(self):
        if self.msg_value == "toggle":
            print("now toggle")
            self.inst_GPIO.toggle_pin()


class MQTT_PUB:
    def __init__(self):
        server = "deskminiserver"
        self.client_pub = MQTTClient("umqtt_client_pub_1", server)
        self.client_pub.connect()

    def mqtt_pub(self, pub_topic="", pub_value=""):
        try:
            self.client_pub.publish(pub_topic, str(pub_value))
        except:
            self.client_pub.disconnect()

if __name__ == "__main__":
    pass
    inst_sub = MQTT_SUB()
    _thread.start_new_thread(inst_sub.mqtt_sub, ())
