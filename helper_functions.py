__author__ = "T. Masiak"
__credits__ = ["-", "-"]
__license__ = "MIT"
__version__ = "0.1"
__maintainer__ = "T. Masiak"
__email__ = "masiaktobias@gmail.com"
__status__ = "Production"

import ujson

#write json parametersto file
json_write = {"ssid":"x", "password":"x"}
json_object = ujson.dumps(json_write)
f = open ("paramters.json", "w")
f.write(json_object)
f.close()

#read json parameters from file
f = open('paramters.json')
obj_read = f.read()
f.close()
json_read = ujson.loads(obj_read)
