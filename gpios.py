__author__ = "T. Masiak"
__credits__ = ["-", "-"]
__license__ = "MIT"
__version__ = "0.1"
__maintainer__ = "T. Masiak"
__email__ = "masiaktobias@gmail.com"
__status__ = "Production"

import machine


class GPIO_CONTROL:
    def __init__(self):
        self.pin_o2 = machine.Pin(2, machine.Pin.OUT)
        self.pin_o2.value(True)

    def toggle_pin(self):
        self.pin_o2.value(not self.pin_o2.value())


if __name__ == "__main__":
    inst_gpio = GPIO_CONTROL()
    inst_gpio.toggle_pin()
